window.pym = require('pym.js');

function log(s) {
	// console.log(JSON.stringify(s, null, 4));
}

(function() {

	var _ = require('lodash');
	var d3 = require('d3');
	var prepareData = require('./prepareData.js');

	var data = require('../data/data.csv');

	var draftOrder = prepareData.draftOrder(data);
	var picks = prepareData.picks(data);

	log(picks);

	// Make the tables with d3
	function makeContentThatDoesntChange() {

		// DATA JOIN
		var tables = d3.select('.tables').selectAll('.table')
			.data(draftOrder, d => d.year);

		// ENTER
		tables.enter().append('div')
			.attr({
				'class': 'table'
			});

		var year = tables.append('div')
			.attr('class', 'year')
			.html(d => `<span class='gamma'>${d.year}</span>`);

		var buttons = tables.append('div')
			.attr('class', 'buttons')
			.html("<button class='before btn btn--primary'>Before</button><button class='after btn btn--primary'>After</button>");

		var picks = tables.append('div')
			.attr('class', 'picks');

		var noChanges = picks.append('div')
			.attr('class', 'no-changes')
			.html(function(d, i) {

				var html = d.order.map(function(a, b, c) {

					return `<div class='no-change row'>
						<div class='number cell'><span class='theta'>${a.number}</span></div>
						<div class='logo cell'><span class='theta'><img src='${a.logo}' /></span></div>
						<div class='team cell'><span class='theta'>${a.team}</span></div>
					</div>`;

				}).join('');

				return html;
			});

		var changes = picks.append('div')
			.attr('class', 'changes');
	}

	makeContentThatDoesntChange();

	function draw(year, _data, isDrawingBefore) {

		// This function gets a year and
		// a dataset, it doesn't know if the picks
		// are before or after.

		// FIRST: we need to locate this year's table.

		var $yearSpan = $(`.year span:contains(${year})`);
		var changes = $yearSpan.parents('.table').find('.picks .changes').get(0);

		// DATA JOIN
		var divs = d3.select(changes).selectAll('div.change')
			.data(_data, d => d.player);

		// UPDATE
		divs
			.style({
				'z-index': function(d) {
					return d.order;
				}
			})
			.transition()
			.duration(2000)
			.delay(function(datum, index) {
				return index * 1000;
			})
			.style({
				'top': function(d) {
					return ((d.order - 1) * 51) + 'px';
				},
				'opacity': isDrawingBefore ? 0.6 : 1
			})
		// 	.transition()
		// 	.duration(2000)
		// 	.style({
		// 		'top': function(d) {
		// 			return (d.order * 100) + 'px';
		// 		}
		// 	})
		// 	.each('end', function(datum, index) {

		// 		d3.select(this)
		// 			.html(function(d) {
		// 				return '<span>' + d.name + '</span><span>' + d.team + '</span>';
		// 			});
		// 	});

		// ENTER
		divs.enter().append('div')
			.attr('class', 'change row')
			.style({
				'top': function(d) {
					return ((d.order - 1) * 51) + 'px';
				},
				'opacity': isDrawingBefore ? 0.6 : 1
			})
			.html(function(d) {

				var html = `
	                <div class='cell player'><span class='theta'>${d.player}</span></div>
	                <div class='cell headshot'><img src='http://c.o0bg.com/rw/Boston/2011-2020/WebGraphics/Sports/BostonGlobe.com/2015/06/redraft/img/${d.headshot}.jpg' /></div>
                `;
                return html;
			})

		divs
			.exit()
			.remove()
	}

	function drawBefore(year) {

		// STEPS
		// 1: get the before picks for this year

		var thisYearsData = _(picks)
			.filter({year: year})
			.pluck('picks')
			.flatten()
			.map(function(v) {
				return {
					player: v.player,
					headshot: v.headshot,
					order: v.before ? v.before : 15
				}
			})
			.sortBy('order')
			.value();

		log(thisYearsData);

		// 2: take that data and give it to a function
		// that will do the datajoin/update/enter for
		// the right table
		draw(year, thisYearsData, true);
	}

	function drawAfter(year) {

		// STEPS
		// 1: get the after picks for this year

		var thisYearsData = _(picks)
			.filter({year: year})
			.pluck('picks')
			.flatten()
			.sortBy(function(v, i) {
				return v.before		
			})
			.map(function(v) {
				return {
					player: v.player,
					headshot: v.headshot,
					order: v.after ? v.after : 15
				}
			})
			.reject({order: null})
			.value();

		log(thisYearsData);

		// 2: take that data and give it to a function
		// that will do the datajoin/update/enter for
		// the right table
		draw(year, thisYearsData, false);
	}

	function wireButtons() {

		var DISABLED = 'btn--disabled';

		$('button.before').on('click', function(d) {
			var year = $(this).parents('.table').find('.year span').html();
			drawBefore(year);

			// When I click this button,
			// this button should be disabled,
			// and its sibling should get undisabled
			$(this).addClass(DISABLED);
			$(this).siblings().removeClass(DISABLED);
		});

		$('button.after').on('click', function(d) {
			var year = $(this).parents('.table').find('.year span').html();
			drawAfter(year);

			// When I click this button,
			// this button should be disabled,
			// and its sibling should get undisabled
			$(this).addClass(DISABLED);
			$(this).siblings().removeClass(DISABLED);
		});

	}

	function makeContentThatChanges() {

		wireButtons();

		$('button.before').click();
	}

	makeContentThatChanges();

})();
