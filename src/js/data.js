var data = [
	{
		"name": "gabriel",
		"color": "red",
		"before": {
			"order": 0,
			"team": "celtics"
		},
		"after": {
			"order": 4,
			"team": "wizards"
		}
	},
	{
		"name": "luke",
		"color": "blue",
		"before": {
			"order": 1,
			"team": "blue"
		},
		"after": {
			"order": 3,
			"team": "green"
		}
	},
	{
		"name": "russell",
		"color": "yellow",
		"before": {
			"order": 2,
			"team": "yellow"
		},
		"after": {
			"order": 2,
			"team": "red"
		}
	},
	{
		"name": "james",
		"color": "orange",
		"before": {
			"order": 3,
			"team": "orange"
		},
		"after": {
			"order": 1,
			"team": "blue"
		}
	},
	{
		"name": "patrick",
		"color": "green",
		"before": {
			"order": 4,
			"team": "green"
		},
		"after": {
			"order": 0,
			"team": "yellow"
		}
	},
];

module.exports = data;