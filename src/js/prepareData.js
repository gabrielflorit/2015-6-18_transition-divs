module.exports = {
	draftOrder: function(data) {

		var draftOrder = _(data)
			.groupBy('year')
			.map(function(v, i) {

				var order = _(v)
					.groupBy('number')
					.map(function(v, i) {
						return {
							number: +i,
							team: v[0].team,
							logo: v[0].logo
						}
					})
					.value();

				return {
					year: +i,
					order
				};
			})
			.value();

		return draftOrder;
	},

	picks: function(data) {

		var picks = _(data)
			.groupBy('year')
			.map(function(v, i) {

				var picks = _(v)
					.groupBy('player')
					.map(function(v, i) {

						var beforeProperties = _.find(v, {type: 'then'});
						var beforeNumber = beforeProperties ? +beforeProperties.number : null;

						var afterProperties = _.find(v, {type: 'now'});
						var afterNumber = afterProperties ? +afterProperties.number : null;

						return {
							player: i,
							headshot: v[0].headshot,
							before: beforeNumber,
							after: afterNumber
						};
					})
					.value();

				return {
					year: i,
					picks
				}



			})
			.value();

		return picks;

	}
};